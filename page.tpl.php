<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>">

<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?>
<style type="text/css" media="screen">@import "<?php print base_path(). path_to_theme(); ?>/siteground.css";</style>
<style type="text/css" media="projection">@import "<?php print base_path(). path_to_theme(); ?>/siteground.css";</style>
<style type="text/css" media="print">@import "<?php print base_path(). path_to_theme(); ?>/print.css";</style>
<?php print $styles ?>
<?php print $scripts ?>
</head>

<body>
  <div id="bg">
  <div id="wrap">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td id="header">	
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="800" height="115">
  <param name="movie" value="<?php print base_path(). path_to_theme(); ?>/images/siteground.swf" />
  <param name="menu" value="false" />
  <param name="quality" value="best" />  
  <embed src="<?php print base_path(). path_to_theme(); ?>/images/siteground.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="800" height="115"></embed>
</object>		
		</td>
  	</tr>
</table> 
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr class="topbg">
	    <td class="primary-links" align="left" width="50%">
	      <?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?>
	    </td>
		<td align="right" width="50%">
      <?php print $search_box ?>
	    </td>
  	</tr>
</table>		
<table id="content" border="0" cellpadding="15" cellspacing="0" width="100%">
  <tr>
    <?php if ($sidebar_left != ""): ?>
    <td id="sidebar-left">
      <?php print $sidebar_left ?>
      <? $sg = 'banner'; include "templates.php";?>
    </td>
    <?php endif; ?>

    <td valign="top">
      <?php if ($mission != ""): ?>
      <div id="mission"><?php print $mission ?></div>
      <?php endif; ?>

      <div id="main">
        <?php if ($title != ""): ?>
          <?php print $breadcrumb ?>
          <h1 class="title"><?php print $title ?></h1>

          <?php if ($tabs != ""): ?>
            <div class="tabs"><?php print $tabs ?></div>
          <?php endif; ?>

        <?php endif; ?>

        <?php if ($help != ""): ?>
            <div id="help"><?php print $help ?></div>
        <?php endif; ?>

        <?php if ($messages != ""): ?>
          <?php print $messages ?>
        <?php endif; ?>

      <!-- start main content -->
      <?php print $content; ?>
      <?php print $feed_icons; ?>
      <!-- end main content -->

      </div><!-- main -->

<div id="footer-message">
<? $sg = ''; include "templates.php";?>
</div>
    </td>
    <?php if ($sidebar_right != ""): ?>
    <td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td>
    <?php endif; ?>
  </tr>
</table>
<?php print $closure;?>
</div>
</div>
<div id="footer"></div>
</body>
</html>
